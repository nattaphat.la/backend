'use strict';
const app = require('../../server/server');

module.exports = function(Appuser) {
  Appuser.afterRemote('login', async (ctx, accessToken) => {
    const user = await accessToken.user.get();
    const roles = await user.roles.find();
    accessToken.__data.roles = roles;
    return Promise.resolve();
  });
  Appuser.observe('before save', ctx => {
    const {role} = ctx.instance || ctx.data;
    ctx.hookState.role = role;
    return Promise.resolve();
  });
  Appuser.observe('after save', async (ctx) => {
    const {instance} = ctx;
    if (!ctx.hookState.role) {
      return Promise.resolve();
    }
    try {
      await instance.assignRole(ctx.hookState.role);
      return Promise.resolve();
    } catch (e) {
      return Promise.reject(e);
    }
  });
  Appuser.afterRemote('findById', async (ctx, user) => {
    try {
      const roles = await user.roles.find();
      if (roles.length > 0) {
        user.__data.role = roles[0].id;
      }
      return Promise.resolve();
    } catch (e) {
      return Promise.reject(e);
    }
  });
  Appuser.prototype.assignRole = async function(roleId) {
    const {User, Role, RoleMapping} = app.models;
    try {
      const user = this;
      const role = await Role.findById(roleId);
      if (!role) {
        return Promise.reject('ROLE_NOTFOUND');
      }
      const res = await RoleMapping.destroyAll({
        principalId: user.id,
        roleId: {
          neq: roleId,
        },
        principalType: RoleMapping.USER,
      });
      const roleMapping = await RoleMapping.findOrCreate({
        principalType: RoleMapping.USER,
        principalId: user.id,
        roleId,
      });
      return Promise.resolve(roleMapping);
    } catch (e) {
      return Promise.reject(e);
    }
  };
};
