'use strict';

module.exports = {
  db: {
    name: 'db',
    connector: 'postgresql',
    url: process.env.DATABASE_URL ||
          'postgres://greendrive:mydefaultpassword@postgresdb:5432/greendrive',
  },
};
