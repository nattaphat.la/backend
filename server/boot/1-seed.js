'use strict';
const roles = [
  'admin',
  'logistic',
  'wasteGenerator',
  'wasteDisposal',
  'broker',
];
const users = [{
  username: 'administrator',
  email: 'admin@backend.com',
  password: 'password',
  emailVerified: true,
  role: 'admin',
}];
module.exports = async function(app) {
  const User = app.models.AppUser;
  const Role = app.models.Role;
  const RoleMapping = app.models.RoleMapping;
  async function createRole() {
    try {
      let results = await Promise.all(roles.map(async (role) => {
        let [model, created] = await Role.findOrCreate(
          {
            where: {
              name: role,
            },
          },
          {
            name: role,
          }
        );
        return model;
      }));
      return Promise.resolve(results);
    } catch (e) {
      return Promise.reject(e);
    }
  }
  async function createUser(roles) {
    try {
      let results = await Promise.all(users.map(async (user) => {
        let [model, created] = await User.findOrCreate(
          {
            where: {
              username: user.username,
            },
          }, {
            ...user,
            role: roles.find(o => o.name === user.role).id,
          });
        return model;
      }));
      return Promise.resolve(results);
    } catch (e) {
      return Promise.reject(e);
    }
  }
  // async function assignRole(user, role) {
  //   try {
  //     let roleMapping = await role.principals.findOne({
  //       where: {
  //         principalType: RoleMapping.USER,
  //         principalId: user.id,
  //         roleId: role.id,
  //       },
  //     });
  //     if (!roleMapping) {
  //       roleMapping = await role.principals.create({
  //         principalType: RoleMapping.USER,
  //         principalId: user.id,
  //       });
  //     }
  //     return Promise.resolve(roleMapping);
  //   } catch (e) {
  //     return Promise.reject(e);
  //   }
  // }
  try {
    const roles = await createRole();
    await createUser(roles);
    // var {model: _adminUser, created: _isAdminCreated} = await createAdmin();
    // var _adminRole = _roles.find((role) => {
    //   return role.name === 'admin';
    // });
    // let result = await assignRole(_adminUser, _adminRole);
  } catch (e) {
    throw e;
  }
};
