FROM node:8-alpine
COPY . /app
WORKDIR /app
RUN npm install
ENV NODE_ENV=production
CMD [ "node", "." ]